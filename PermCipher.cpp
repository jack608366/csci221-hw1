#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <cstring>
using namespace std;

int main(int argc, char* args[]) {

    string mode;
    fstream inFile;
    string inFileName;
    fstream outFile;
    string outFileName;
    int n;
    string perm;
    
    if (argc == 1) {
        cout << "Usage: ./PermCipher option -i InputFileName -o OutputFileName\nOptions: -i Encrypt\n         -d Decrypt\n";
	return 0;
        }	

    for (int i = 0; i < argc - 1; i++){
	string arg = args[i];
	string nextArg = args[i + 1];
	if (arg == "-e") {
		mode = "Encrypt";
	}
	else if (arg == "-d") {
		mode = "Decrypt";
	}
	else if (arg == "-i") {
		if (mode == "Encrypt") {
			inFile.open(nextArg, ios::in);
		}
		else
			inFile.open(nextArg, ios::in | ios::binary | ios::ate);
		inFileName = nextArg;
	}
	else if (arg == "-o") {
		if (mode == "Encrypt") {
			outFile.open(nextArg, ios::out | ios::binary);
		}
		else
			outFile.open(nextArg, ios::out);
		outFileName = nextArg;
	}
    }

    cout << "Welcome to the Permutation Cipher \n";
    cout << "Selected mode: " << mode;
    cout << "\nInput File: " << inFileName;
    cout << "\nOutput File: " << outFileName;
    cout << "\nPlease enter block size (2-8) and the permutation (e.g., 4 2143): ";
    cin >> n >> perm;

    if (2 > n > 8) {
	cout << "\nInvalid block size please make enter int between 2 and 8";
    }

    if (n != perm.size()) {
	cout << "\nBlock size does not match length of permuation";
    }

    if (mode == "Encrypt") {
	if (inFile.is_open()) {
		char ch;
		string in;
		string out;
		while (inFile >> noskipws >> ch) {
			in += ch;
			out += 'x';
		}
		for (int i = 0; i < in.size() % n; i++) {
			in += 'x';
			out += 'x';
		}
		cout << "\n input: " << in;
		for (int i = 0; i < in.size(); i += n) {
			for (int ii = 0; ii < n; ii++) {
				out[(int(perm[ii]) + i - 49)] = in[i + ii];
			}
		}

		cout << "\n output: " << out;
		if (outFile.is_open()) {
			outFile.write(out.c_str(), out.size());
			outFile.close();
		}
		else
			cout << "Unable to open output file";

		inFile.close();
	}
	else
		cout << "\n Unable to open input file";
    }
    else if (mode == "Decrypt") {	
	if (inFile.is_open()) {
		char* in;
		string out;
		streampos size;
		size = inFile.tellg();
		in = new char[size];
		inFile.seekg(0, ios::beg);
		inFile.read(in, size);
		inFile.close();
		for (int i = 0; i < size; i ++) {
			out += 'x';
		}
		cout << "\n input: " << in;
		for (int i = 0; i < size; i += n) {
			for (int ii = 0; ii < n; ii++) {
			out[i + ii] = in[(int(perm[ii]) + i - 49)];
			}
		}
		cout << "\n output: " << out;
		fstream myfile("myfile", ios::out | ios::binary);
		if (outFile.is_open()) {
			outFile.write(out.c_str(), out.size());
			outFile.close();
		}
		else
			cout << "Unable to open output file";
		inFile.close();
	}
	else
		cout << "\n Unable to open input file";
    }
    else
	    cout << "no mode selected";
    return 0;
}
